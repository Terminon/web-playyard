Dem einzublendenden Element wird ein
animation attribut hinzugefügt, und keyframes definiert:

Die Parametrisierung mit einer CSS Variable ist optional.



nav {
	--nav-load-time:300ms;
    animation: nav-load var(--nav-load-time) ease-in;
}


@keyframes nav-load {
    0% {
        transform: translateY(-100%);
    }

    100% {
        transform: translateY(0);
    }
}