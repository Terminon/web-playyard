Couldnt properly process changing font sizes.

Current implementation does not use CSS variable --arrow-offset 
as i was not able to put a proper computation into the
translate command.

Anyway, current version should work for font sizes between 
100% and 150%. Just the arrow will stick out a little more
or less.
