const tabs = document.querySelectorAll("[data-tab-target");
const tabContents = document.querySelectorAll("[data-tab-content]");

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    /* 
    Dashes in data property are converted to CamelCase: https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes

    data-tab-target in markup => dataset.tabTarget in JavaScript
    */
    console.log("Click");
    const target = document.querySelector(tab.dataset.tabTarget);

    tabContents.forEach((tabContent) => tabContent.classList.remove("active"));
    /* Multi command version:
    tabContents.forEach(function(tabContent) {
        tabContent.classList.remove('active'));
    })
    */

    target.classList.add("active");
  });
});
