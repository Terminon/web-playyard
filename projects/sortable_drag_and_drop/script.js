const draggables = document.querySelectorAll(".draggable");
const containers = document.querySelectorAll(".container");

draggables.forEach((draggable) => {
  draggable.addEventListener("dragstart", () => {
    draggable.classList.add("dragging");
  });

  draggable.addEventListener("dragend", () => {
    draggable.classList.remove("dragging");
  });
});

containers.forEach((container) => {
  container.addEventListener("dragover", (e) => {
    e.preventDefault();
    const afterElement = getDragAfterElement(container, e.clientY);
    const draggable = document.querySelector(".dragging");
    if (afterElement == null) {
      container.appendChild(draggable);
    } else {
      container.insertBefore(draggable, afterElement);
    }
  });
});

function getDragAfterElement(container, y) {
  /* 
    The spread operator (...) converts the nodelist result of
    querySelectorAll, and converts it to an array, so that we
    can use JavaScript array methods on it.
    https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/Spread_operator
  */
  const draggableElements = [
    ...container.querySelectorAll(".draggable:not(.dragging)"),
  ];

  /* Reduce draggableElements to the closest Value. Loop Var is child, final storage is closest.
    For convenience, closest does not only contain the currently selected child,
    but also its y-offset, to be used in the next comparison, so offset calculation will
    only be needed for the next new child.

    https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
    */
  return draggableElements.reduce(
    (closest, child) => {
      const box = child.getBoundingClientRect();
      const offset = y - box.top - box.height / 2;
      if (offset < 0 && offset > closest.offset) {
        return { offset: offset, element: child };
      } else {
        return closest;
      }
    },
    { offset: Number.NEGATIVE_INFINITY } // Initial closest value: infinitely far away
  ).element;
}
