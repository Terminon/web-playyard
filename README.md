
### What is this repository for? ###

This a collection of web-snippets, documenting usage of CSS ad Javascript
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Every directory should contain index.html, style.css and script.js
Just serve these files on a server. Brackets can do this out of
the box, while in VS Code you have to install Live Server extension
Or you serve the directory via this python one-line:

python -m http.server 8000

..or use any one-liner serve statement from these options:
https://gist.github.com/willurd/5720255

